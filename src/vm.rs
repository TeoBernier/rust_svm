use crate::prelude::*;

pub struct VM {
    pub debug: bool,
    pub globs: Vec<Value>,
    pub stack: Vec<Value>,
    pub frames: List<Box<Frame>>,
    pub program: Vec<isize>,
    pub gc: GC,
}

impl VM {
    const STACK_SIZE: usize = 256;
    const GLOBS_SIZE: usize = 256;
    pub const DEFAULT_GC_FREQUENCY: usize = 256;

    pub fn new(
        program: Vec<isize>,
        debug_vm: bool,
        debug_gc: bool,
        collection_frequency: usize,
    ) -> Self {
        let mut gc = GC::new(debug_gc, collection_frequency);
        let env = gc.alloc_env(0);

        let frame = Box::new(Frame {
            env,
            sp: 0,
            pc: 0,
            caller_frame: None,
        });

        let mut vm = VM {
            debug: debug_vm,
            program,
            globs: Vec::with_capacity(Self::GLOBS_SIZE),
            stack: Vec::with_capacity(Self::STACK_SIZE),
            gc,
            frames: List { head: Some(frame) },
        };
        vm.globs.push(Unit);
        vm
    }

    pub fn next_bc(&mut self) -> isize {
        let next = self.frames.utop().pc;
        self.frames.utop_mut().pc += 1;
        self.program[next]
    }

    pub fn execute_instr(&mut self, instr: isize) {
        if self.debug {
            print!("=== Execute next intruction ===\n>>> ");
            bytecode_print_instr(&self.program, self.frames.utop().pc - 1);
        }

        match Instruction::from(instr) {
            GAlloc => self.globs.push(Unit),
            GStore => {
                let idx = self.next_bc() as usize;
                self.globs[idx] = self.stack.pop().unwrap();
            }
            GFetch => {
                let idx = self.next_bc() as usize;
                self.stack.push(self.globs[idx]);
            }
            Store => {
                let idx = self.next_bc() as usize;
                let value = self.stack.pop().unwrap();
                unsafe { self.frames.utop_mut().env.get_mut()[idx] = value };
            }
            Fetch => {
                let idx = self.next_bc() as usize;
                self.stack
                    .push(unsafe { self.frames.utop().env.get_ref()[idx] });
            }
            Push => {
                let val = match ValueType::from(self.next_bc()) {
                    UnitType => Unit,
                    IntType => Int(self.next_bc()),
                    FunType => Fun {
                        env: self.frames.utop().env,
                        pc: self.next_bc() as usize,
                    },

                    FunNType => FunN {
                        env: self.frames.utop().env,
                        pc: self.next_bc() as usize,
                        min_args: self.next_bc() as usize,
                    },

                    PrimType => Prim(PrimFun::from(self.next_bc())),
                    BoolType => Bool(self.next_bc() == 1),
                    _ => panic!(
                        "Unknow type: '{}' (in push)",
                        self.program[self.frames.utop().pc - 1]
                    ),
                };
                self.stack.push(val);
            }
            Pop => {
                let val = self.stack.pop().unwrap();
                if self.debug {
                    print!("DISPLAY> ");
                }
                println!("{:?}", val);
            }
            Call => {
                let fun = self.stack.pop().unwrap();
                let nb_args = self.next_bc() as usize;
                assert!(self.stack.len() >= nb_args);

                if let Prim(fun) = fun {
                    fun.exec(self, nb_args);
                } else {
                    let (env, pc) = match fun {
                        Fun { env, pc } => {
                            let mut nenv = self.gc.alloc_env(0);

                            unsafe {
                                nenv.get_mut().next = Some(env);
                                nenv.get_mut().content =
                                    self.stack.drain(self.stack.len() - nb_args..).collect();
                            }

                            (nenv, pc)
                        }

                        FunN { env, pc, min_args } => {
                            let mut nenv = self.gc.alloc_env(0);
                            let mut prec = EmptyList;
                            for elem in self
                                .stack
                                .drain(self.stack.len() - nb_args..self.stack.len() - min_args)
                            {
                                let mut pair = self.gc.alloc_pair();
                                unsafe {
                                    pair.get_mut().car = elem;
                                    pair.get_mut().cdr = prec;
                                }

                                prec = PairRef(pair);
                            }

                            unsafe {
                                nenv.get_mut().content.push(prec);
                                nenv.get_mut().next = Some(env);
                                for elem in self.stack.drain(self.stack.len() - min_args..) {
                                    nenv.get_mut().content.push(elem);
                                }
                            }
                            (nenv, pc)
                        }

                        fun => panic!("Unknow value: '{:?}' (in call)", fun),
                    };
                    self.push_frame(env, pc);
                }
            }
            Return => {
                let old_frame = self.pop_frame();
                match self.stack.len() - old_frame.sp {
                    0 => self.stack.push(Unit),
                    1 => (),
                    _ => {
                        self.stack[old_frame.sp] = self.stack.pop().unwrap();
                        self.stack.truncate(old_frame.sp + 1);
                    }
                }
            }
            Jump => {
                let n_pc = self.next_bc() as usize;
                self.frames.utop_mut().pc = n_pc;
            }

            JFalse => {
                if Bool(false) == self.stack.pop().unwrap() {
                    let npc = self.next_bc() as usize;
                    self.frames.utop_mut().pc = npc;
                } else {
                    self.next_bc();
                }
            }
            Error => {
                panic!("Error number {}", self.next_bc())
            }

            Alloc => {
                let capacity = self.next_bc() as usize;
                let mut nenv = self.gc.alloc_env(capacity);
                unsafe {
                    nenv.get_mut().next = Some(self.frames.utop().env);
                }
                self.frames.utop_mut().env = nenv;
            }

            Delete => {
                self.frames.utop_mut().env =
                    unsafe { self.frames.utop().env.get_ref().next.unwrap() };
            }

            _ => panic!("Unknow opcode: {}", self.program[self.frames.utop().pc - 1]),
        }
        if self.debug {
            println!(
                "State:\n\tPC = {}\n\tGlobals = {:?}\n\tStack = {:?}\n\tFrame = {:?}",
                self.frames.utop().pc,
                self.globs,
                self.stack,
                self.frames.utop()
            )
        }
    }

    pub fn execute(&mut self) {
        let mut instruction_counter = 0;
        if self.debug {
            println!(
                "Initial State:\n\tPC = {}\n\tGlobals = {:?}\n\tStack = {:?}\n\tFrame = {:?}",
                self.frames.utop().pc,
                self.globs,
                self.stack,
                self.frames.utop()
            )
        }

        while self.frames.utop().pc < self.program.len() {
            let instruction = self.next_bc();
            self.execute_instr(instruction);

            instruction_counter += 1;

            if instruction_counter == self.gc.collection_frequency {
                GC::collect(self);
                instruction_counter = 0;
            }
        }
        GC::collect(self);

        if self.debug {
            println!(
                "Final State:\n\tPC = {}\n\tGlobals = {:?}\n\tStack = {:?}\n\tFrame = {:?}",
                self.frames.utop().pc,
                self.globs,
                self.stack,
                self.frames.utop()
            )
        }
    }

    pub fn push_frame(&mut self, env: MemCell<Env>, pc: usize) {
        let frame = Box::new(Frame {
            env,
            pc,
            sp: self.stack.len(),
            caller_frame: None,
        });
        self.frames.push(frame);
    }

    pub fn pop_frame(&mut self) -> Box<Frame> {
        self.frames.upop()
    }
}
