use super::prelude::*;

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Value {
    Unit,

    Int(isize),

    Prim(PrimFun),

    Fun {
        pc: usize,
        env: MemCell<Env>,
    },

    Bool(bool),

    PairRef(MemCell<Pair>),

    FunN {
        pc: usize,
        min_args: usize,
        env: MemCell<Env>,
    },

    EmptyList,
}

impl Value {
    pub fn is_list(&self) -> bool {
        matches!(self, EmptyList)
            || if let PairRef(pair_ref) = self {
                unsafe { pair_ref.get_ref().cdr.is_list() }
            } else {
                false
            }
    }
}

impl Default for Value {
    fn default() -> Self {
        Value::Unit
    }
}

#[derive(Clone, Copy)]
pub struct Pair {
    pub car: Value,
    pub cdr: Value,
}

impl Pair {
    fn print_cdr(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.cdr {
            EmptyList => write!(f, ")"),
            PairRef(pair_ref) => {
                let pair = unsafe { pair_ref.get_ref() };
                write!(f, " {:?}", pair.car)?;
                pair.print_cdr(f)
            }
            value => {
                write!(f, " . {:?})", value)
            }
        }
    }
}

use std::fmt::{self, Debug, Formatter};

impl Debug for Pair {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "({:?}", self.car)?;
        self.print_cdr(f)
    }
}

impl Default for Pair {
    fn default() -> Self {
        Pair {
            car: Unit,
            cdr: Unit,
        }
    }
}
impl Debug for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Unit => write!(f, "Unit"),
            Int(x) => write!(f, "{}", x),
            Prim(prim) => write!(f, "{:?}", prim),
            Bool(b) => write!(f, "{}", b),
            Fun { pc, .. } => write!(f, "Closure@{}", pc),
            PairRef(pair) => Debug::fmt(unsafe { pair.get_ref() }, f),
            FunN { pc, min_args, .. } => write!(f, "Closure_n@{} | min args : {}", pc, min_args),
            EmptyList => write!(f, "()"),
        }
    }
}
