pub use crate::prelude::*;
use std::ops::RangeBounds;

pub use Instruction::*;

#[repr(isize)]
#[derive(Debug)]
pub enum Instruction {
    GAlloc = 0,
    Push,
    GStore,
    Pop,
    Jump,
    GFetch,
    Call,
    Return,
    Fetch,
    JFalse,
    Store,
    Error,
    Alloc,
    Delete,

    UnknownInstr,

    Begin = 424242,
}

impl Instruction {
    const FIRST_VAL: isize = GAlloc as isize;
    const LAST_VAL: isize = UnknownInstr as isize - 1;
    const SPECIAL_VAL: isize = Begin as isize;
}

impl From<isize> for Instruction {
    fn from(instruction: isize) -> Self {
        match instruction {
            Self::FIRST_VAL..=Self::LAST_VAL | Self::SPECIAL_VAL => unsafe {
                std::mem::transmute(instruction)
            },
            _ => UnknownInstr,
        }
    }
}

pub use PrimFun::*;

#[repr(isize)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PrimFun {
    Add = 0,
    Sub,
    Mul,
    Div,
    Eq,
    List,
    Cons,
    Car,
    Cdr,
    ZeroP,
    Newline,
    Display,
    SetCar,
    SetCdr,
    NullP,
    ListP,
    PairP,

    UnknownPrim,
}

fn arith_drain<R: RangeBounds<usize>, F: Fn(isize, isize) -> isize>(
    range: R,
    stack: &mut Vec<Value>,
    base: isize,
    f: F,
) -> isize {
    stack
        .drain(range)
        .map(|value| {
            if let Int(x) = value {
                x
            } else {
                panic!("Cannot execute arithmetic primitive with '{:?}'", value);
            }
        })
        .fold(base, f)
}

impl PrimFun {
    const FIRST_VAL: isize = Add as isize;
    const LAST_VAL: isize = UnknownPrim as isize - 1;

    pub fn exec(self, vm: &mut VM, nb_args: usize) {
        match self {
            Add => match nb_args {
                0 => vm.stack.push(Int(0)),
                1 => (),
                n => {
                    let res =
                        arith_drain(vm.stack.len() - n.., &mut vm.stack, 0, std::ops::Add::add);
                    vm.stack.push(Int(res));
                }
            },
            Sub => match nb_args {
                0 => vm.stack.push(Int(0)),
                1 => (),
                n => match vm.stack.pop() {
                    Some(Int(res)) => {
                        let res = arith_drain(
                            vm.stack.len() + 1 - n..,
                            &mut vm.stack,
                            res,
                            std::ops::Sub::sub,
                        );
                        vm.stack.push(Int(res));
                    }
                    value => panic!("Cannot execute arithmetic primitive with '{:?}'", value),
                },
            },
            Mul => match nb_args {
                0 => vm.stack.push(Int(1)),
                1 => (),
                n => {
                    let res =
                        arith_drain(vm.stack.len() - n.., &mut vm.stack, 1, std::ops::Mul::mul);
                    vm.stack.push(Int(res));
                }
            },
            Div => match nb_args {
                0 => vm.stack.push(Int(0)),
                1 => (),
                n => match vm.stack.pop() {
                    Some(Int(res)) => {
                        let res = arith_drain(
                            vm.stack.len() + 1 - n..,
                            &mut vm.stack,
                            res,
                            std::ops::Div::div,
                        );
                        vm.stack.push(Int(res));
                    }
                    value => panic!("Cannot execute arithmetic primitive with '{:?}'", value),
                },
            },
            Eq => {
                let a = vm.stack.pop().unwrap();
                let b = vm.stack.pop().unwrap();
                if std::mem::discriminant(&a) == std::mem::discriminant(&b) {
                    match a {
                        Int(..) | Bool(..) => {
                            let res = Bool(a == b);
                            vm.stack.push(res);
                        }
                        _ => panic!("Error : eq? is not implemented for '{:?} == {:?}' !", a, b),
                    }
                } else {
                    vm.stack.push(Bool(false));
                }
            }
            List => {
                let gc = &mut vm.gc;
                let res =
                    vm.stack
                        .drain(vm.stack.len() - nb_args..)
                        .fold(EmptyList, |list, elem| {
                            let mut pair_ref = gc.alloc_pair();
                            let pair = unsafe { pair_ref.get_mut() };
                            pair.cdr = list;
                            pair.car = elem;
                            PairRef(pair_ref)
                        });
                vm.stack.push(res);
            }
            Cons => {
                let mut pair_ref = vm.gc.alloc_pair();
                let pair = unsafe { pair_ref.get_mut() };
                pair.car = vm.stack.pop().unwrap();
                pair.cdr = vm.stack.pop().unwrap();
                vm.stack.push(PairRef(pair_ref));
            }
            Car => {
                let value = vm.stack.pop().unwrap();
                if let PairRef(pair_ref) = value {
                    vm.stack.push(unsafe { pair_ref.get_ref().car });
                } else {
                    panic!("Error cannot get car of '{:?}' !", value);
                }
            }
            Cdr => {
                let value = vm.stack.pop().unwrap();
                if let PairRef(pair_ref) = value {
                    vm.stack.push(unsafe { pair_ref.get_ref().cdr });
                } else {
                    panic!("Error cannot get cdr of '{:?}' !", value);
                }
            }
            ZeroP => {
                let value = vm.stack.pop().unwrap();
                vm.stack.push(Bool(value == Int(0)));
            }
            Newline => println!(),
            Display => print!("{:?}", vm.stack.pop().unwrap()),
            SetCar => {
                let value = vm.stack.pop().unwrap();
                let car = vm.stack.pop().unwrap();
                if let PairRef(mut pair_ref) = value {
                    unsafe {
                        pair_ref.get_mut().car = car;
                    }
                } else {
                    panic!("Cannot set car of '{:?}' !", value)
                }
            }
            SetCdr => {
                let value = vm.stack.pop().unwrap();
                let cdr = vm.stack.pop().unwrap();
                if let PairRef(mut pair_ref) = value {
                    unsafe {
                        pair_ref.get_mut().cdr = cdr;
                    }
                } else {
                    panic!("Cannot set cdr of '{:?}' !", value)
                }
            }
            NullP => {
                let value = vm.stack.pop().unwrap();
                vm.stack.push(Bool(value == EmptyList));
            }
            ListP => {
                let value = vm.stack.pop().unwrap();
                vm.stack.push(Bool(value.is_list()));
            }
            PairP => {
                let value = vm.stack.pop().unwrap();
                vm.stack.push(Bool(matches!(value, PairRef(..))))
            }
            _ => unreachable!(),
        }
        match self {
            Newline | Display | SetCar | SetCdr => vm.stack.push(Unit),
            _ => (),
        }
    }
}

impl From<isize> for PrimFun {
    fn from(prim_num: isize) -> Self {
        match prim_num {
            Self::FIRST_VAL..=Self::LAST_VAL => unsafe { std::mem::transmute(prim_num) },
            _ => UnknownPrim,
        }
    }
}

pub use ValueType::*;

#[repr(isize)]
#[derive(Debug, Clone, Copy)]
pub enum ValueType {
    UnitType = 0,
    IntType,
    PrimType,
    FunType,
    BoolType,
    PairType,
    FunNType,

    UnknownType,
}

impl ValueType {
    const FIRST_VAL: isize = UnitType as isize;
    const LAST_VAL: isize = UnknownType as isize - 1;
}

impl From<isize> for ValueType {
    fn from(type_num: isize) -> Self {
        match type_num {
            Self::FIRST_VAL..=Self::LAST_VAL => unsafe { std::mem::transmute(type_num) },
            _ => UnknownType,
        }
    }
}

pub fn bytecode_print_instr(program: &[isize], pc: usize) -> usize {
    let instruction = Instruction::from(program[pc]);
    match instruction {
        GAlloc | Pop | Return | Error | Delete => {
            println!("{:?}", instruction);
            pc + 1
        }
        GStore | Store | GFetch | Fetch | Jump | JFalse | Alloc | Call => {
            println!("{:?} {}", instruction, program[pc + 1]);
            pc + 2
        }
        Push => {
            print!("Push ");
            let value_type = ValueType::from(program[pc + 1]);
            match value_type {
                BoolType => match program[pc + 2] {
                    0 => println!("Bool False"),
                    1 => println!("Bool True"),
                    err => panic!("Error: wrong BOOL value '{}' for PUSH", err),
                },
                IntType => println!("Int {}", program[pc + 2]),
                UnitType => println!("Unit"),
                PrimType => println!("Prim {:?}", PrimFun::from(program[pc + 2])),
                FunType => println!("Fun | pc = {}", program[pc + 2]),
                FunNType => println!(
                    "Fun | pc = {} | min args = {}",
                    program[pc + 2],
                    program[pc + 3]
                ),
                _ => {
                    panic!("Error: unknown type '{}' for PUSH", program[pc + 1])
                }
            }
            match value_type {
                UnitType => pc + 2,
                IntType | BoolType | PrimType | FunType => pc + 3,
                FunNType => pc + 4,
                _ => unreachable!(),
            }
        }
        _ => panic!("Error: unknown opcode '{}'", program[pc]),
    }
}

pub fn bytecode_print(program: &[isize]) {
    let mut pc = 0;
    while pc < program.len() {
        print!("{:4 }: ", pc);
        pc = bytecode_print_instr(&program, pc);
    }
}
