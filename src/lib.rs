#![allow(clippy::upper_case_acronyms)]

pub mod chained_list;
pub mod constant;
pub mod env;
pub mod frame;
pub mod gc;
pub mod utils;
pub mod value;
pub mod vm;

pub mod prelude {
    pub use crate::chained_list::*;
    pub use crate::constant::*;
    pub use crate::env::*;
    pub use crate::frame::*;
    pub use crate::gc::{Mark::*, *};
    pub use crate::utils::*;
    pub use crate::value::{Value::*, *};
    pub use crate::vm::*;
}
