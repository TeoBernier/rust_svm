pub use super::prelude::*;
use std::fmt::{self, Debug, Formatter};

#[derive(Clone, Default)]
pub struct Env {
    pub content: Vec<Value>,
    pub next: Option<MemCell<Env>>,
}

impl Env {
    pub fn new(capacity: usize) -> Env {
        Env {
            content: vec![Unit; capacity],
            next: None,
        }
    }
    fn print_chained(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.content)?;
        match self.next {
            Some(env) => {
                let env = unsafe { env.get_ref() };
                write!(f, " -> ")?;
                env.print_chained(f)
            }
            None => Ok(()),
        }
    }
}

impl Debug for Env {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Env : ")?;
        self.print_chained(f)
    }
}

use std::ops::{Index, IndexMut};

impl Index<usize> for Env {
    type Output = Value;

    fn index(&self, idx: usize) -> &Self::Output {
        if idx >= self.content.len() {
            match self.next {
                Some(ref env) => unsafe { &env.get_ref()[idx - self.content.len()] },
                None => panic!("Error, index outside environment !"),
            }
        } else {
            &self.content[self.content.len() - 1 - idx]
        }
    }
}

impl IndexMut<usize> for Env {
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        if idx >= self.content.len() {
            match self.next {
                Some(ref mut env) => unsafe { &mut env.get_mut()[idx - self.content.len()] },
                None => panic!("Error, index outside environment !"),
            }
        } else {
            let idx = self.content.len() - 1 - idx;
            &mut self.content[idx]
        }
    }
}
