pub mod gc_cell;
pub mod mark;
pub mod mem_cell;

use crate::prelude::*;
pub use gc_cell::GCCell;
pub use mark::Mark;
pub use mem_cell::MemCell;

#[derive(Default, Debug, Clone, Copy)]
pub struct GCData {
    pub mark: Mark,
}

#[derive(Default)]
pub struct GC {
    pub debug: bool,
    pub current_mark: Mark,
    pub heap: List<Box<GCCell>>,
    pub white: List<Box<GCCell>>,
    pub nb_allocated: usize,
    pub collection_frequency: usize,
}

impl GC {
    pub fn new(debug: bool, collection_frequency: usize) -> Self {
        if debug {
            println!("[GC] Initialized with frequency = {}", collection_frequency);
        }
        GC {
            debug,
            collection_frequency,
            heap: List::new(),
            white: List::new(),
            ..GC::default()
        }
    }

    fn default_gc_data(&self) -> GCData {
        GCData {
            mark: self.current_mark,
        }
    }

    fn push_cell(&mut self, cell: GCCell) {
        self.heap.push(Box::new(cell));
    }

    fn pop_cell(&mut self) -> Option<GCCell> {
        self.heap.pop().map(|cell| *cell)
    }

    pub fn alloc_pair(&mut self) -> MemCell<Pair> {
        let pair = MemCell::new(Pair::default(), self.default_gc_data());
        self.push_cell(GCCell::new_pair(pair));
        self.nb_allocated += 1;
        pair
    }

    pub fn alloc_env(&mut self, capacity: usize) -> MemCell<Env> {
        let env = MemCell::new(Env::new(capacity), self.default_gc_data());
        self.push_cell(GCCell::new_env(env));
        self.nb_allocated += 1;
        env
    }

    pub fn mark_and_trace_value(value: &Value, mark: Mark, debug: bool) {
        match value {
            &Fun { env, .. } | &FunN { env, .. } => {
                GC::mark_and_trace_env(env, mark, debug);
            }

            &PairRef(pair) => GC::mark_and_trace_pair(pair, mark, debug),

            _ => (),
        }
    }

    pub fn mark_and_trace_pair(mut pair: MemCell<Pair>, mark: Mark, debug: bool) {
        let old_mark = unsafe { &mut pair.gc_mut().mark };
        if *old_mark != mark {
            if debug {
                println!("[GC]\t\t\t==> pair marked");
            }
            *old_mark = mark;
            let pair = unsafe { pair.get_ref() };
            GC::mark_and_trace_value(&pair.car, mark, debug);
            GC::mark_and_trace_value(&pair.cdr, mark, debug);
        }
    }

    pub fn mark_and_trace_env(mut env: MemCell<Env>, mark: Mark, debug: bool) {
        let old_mark = unsafe { &mut env.gc_mut().mark };
        if *old_mark != mark {
            if debug {
                println!("[GC]\t\t\t==> env marked");
            }
            *old_mark = mark;
            let env = unsafe { env.get_ref() };
            env.content
                .iter()
                .for_each(|value| GC::mark_and_trace_value(value, mark, debug));
            if let Some(env) = env.next {
                GC::mark_and_trace_env(env, mark, debug);
            }
        }
    }

    pub fn mark_and_trace_frame(frame: &Frame, mark: Mark, n: usize, debug: bool) {
        if debug {
            println!("[GC]\t\t\tTracing frame #{}", n);
        }
        GC::mark_and_trace_env(frame.env, mark, debug);
        if let Some(ref frame) = frame.caller_frame {
            GC::mark_and_trace_frame(frame, mark, n + 1, debug);
        }
    }

    pub fn mark_and_trace_roots(vm: &VM, mark: Mark, debug: bool) {
        if debug {
            println!("[GC]\tTracing roots\n[GC]\t\tTracing globals");
        }
        vm.globs
            .iter()
            .for_each(|value| GC::mark_and_trace_value(value, mark, debug));
        if debug {
            println!("[GC]\t\tTracing stack");
        }
        vm.stack
            .iter()
            .for_each(|value| GC::mark_and_trace_value(value, mark, debug));
        if debug {
            println!("[GC]\t\tTracing call frames");
        }
        GC::mark_and_trace_frame(vm.frames.utop(), mark, 0, debug);
    }

    pub fn collect(vm: &mut VM) {
        if vm.gc.debug {
            println!("[GC] Collector started");
        }
        vm.gc.current_mark = !vm.gc.current_mark;

        if vm.gc.debug {
            println!("[GC] Current mark set to : {:?}", vm.gc.current_mark);
        }

        GC::mark_and_trace_roots(vm, vm.gc.current_mark, vm.gc.debug);

        vm.gc.sweep();

        if vm.gc.debug {
            println!("[GC] Collector finished");
        }
    }

    #[allow(clippy::suspicious_map)]
    pub fn old_sweep(&mut self) {
        if self.debug {
            println!("[GC]\tSweep phase started");
        }
        let current_mark = self.current_mark;
        let heap = &mut self.heap;
        let debug = self.debug;
        self.nb_allocated -= heap
            .drain_filter(|cell| unsafe { current_mark != cell.gc_ref().mark })
            .map(|mut cell| {
                if debug {
                    println!(
                        "[GC]\t\tfree cell {:?} : {:?}",
                        cell.get_ptr(),
                        cell.content
                    );
                }

                unsafe { cell.delete() };
            })
            .count();
    }

    #[allow(clippy::suspicious_map)]
    pub fn sweep(&mut self) {
        if self.debug {
            println!("[GC]\tSweep phase started");
        }

        self.white = self.heap.take_list();
        let current_mark = self.current_mark;
        self.heap = self
            .white
            .drain_filter(|cell| unsafe { current_mark == cell.gc_ref().mark })
            .collect();
        if self.debug {
            self.white.iter().for_each(|cell| {
                println!(
                    "[GC]\t\tfree cell {:?} : {:?}",
                    cell.get_ptr(),
                    cell.content
                );
            })
        }
        self.nb_allocated -= self
            .white
            .take_list()
            .into_iter()
            .map(|mut cell| {
                unsafe { cell.delete() };
            })
            .count();
    }
}

impl Drop for GC {
    fn drop(&mut self) {
        while self.pop_cell().is_some() {}
    }
}
