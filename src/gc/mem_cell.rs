use crate::prelude::*;
use std::ptr::{self, NonNull};

/// Structure wrappant un type générique et rajoutant 
/// les données nécéssaires au garbage collector.
#[derive(Debug, Clone)]
pub struct GCBox<T> {
    pub value: T,
    pub gc_data: GCData,
}

#[derive(Clone)]
pub struct MemCell<T> {
    pub ptr: NonNull<GCBox<T>>,
}

impl<T: Clone> Copy for MemCell<T> {}

impl<T> MemCell<T> {
    pub fn new(value: T, gc_data: GCData) -> Self {
        Self {
            ptr: Box::leak(Box::new(GCBox { value, gc_data })).into(),
        }
    }

    ///  Free la pair ou l'env pointé par le pointeur
    /// 
    /// # Safety
    ///  Si déjà free, peut mener à un double free !
    pub unsafe fn free(mut self) {
        ptr::drop_in_place(self.get_mut())
    }

    /// Renvoie une référence sur la pair ou l'env 
    /// 
    /// # Safety
    ///  Si déjà free, peut mener à déréférencer un dangling pointer !
    pub unsafe fn get_ref(&self) -> &T {
        &(*self.ptr.as_ptr()).value
    }

    /// Renvoie une référence mutable sur la pair ou l'env 
    /// 
    ///  # Safety
    ///  Si déjà free, peut mener à déréférencer un dangling pointer !
    pub unsafe fn get_mut(&mut self) -> &mut T {
        &mut (*self.ptr.as_ptr()).value
    }

    /// Renvoie une référence sur les données nécéssaires au garbage collector
    /// 
    ///  # Safety
    ///  Si déjà free, peut mener à déréférencer un dangling pointer !
    pub unsafe fn gc_ref(&self) -> &GCData {
        &(*self.ptr.as_ptr()).gc_data
    }

    /// Renvoie une référence mutable sur les données nécéssaires au garbage collector
    /// 
    ///  # Safety
    ///  Si déjà free, peut mener à déréférencer un dangling pointer !
    pub unsafe fn gc_mut(&mut self) -> &mut GCData {
        &mut (*self.ptr.as_ptr()).gc_data
    }
}

use std::fmt::{self, Debug, Formatter};
impl<T: Debug> Debug for MemCell<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        unsafe { Debug::fmt(self.get_ref(), f) }
    }
}

use std::cmp::{Eq, PartialEq};

impl<T> PartialEq for MemCell<T> {
    /// On implémente l'égalité par pointeur
    fn eq(&self, other: &Self) -> bool {
        self.ptr == other.ptr
    }
}

impl<T> Eq for MemCell<T> {}
