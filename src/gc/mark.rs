#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Mark {
    Red,
    Black,
}

impl Default for Mark {
    fn default() -> Self {
        Mark::Black
    }
}

use std::ops::Not;
impl Not for Mark {
    type Output = Self;

    /// On implémente l'opération !Mark. Ainsi : `!Red -> Black` et `!Black -> Red`
    fn not(self) -> Self::Output {
        use Mark::*;
        match self {
            Red => Black,
            Black => Red,
        }
    }
}