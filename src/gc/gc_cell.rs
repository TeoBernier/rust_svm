use crate::prelude::*;
use GCCellContent::*;

/// Cellule du garbage collector contenant un pointeur vers un env ou une pair
pub struct GCCell {
    pub next: Option<Box<GCCell>>,
    pub content: GCCellContent,
}

impl GCCell {
    /// Crée une nouvelle cell d'une pair
    pub fn new_pair(pair: MemCell<Pair>) -> Self {
        Self {
            next: None,
            content: ContentPair(pair),
        }
    }

    /// Crée une nouvelle cell d'un env
    pub fn new_env(env: MemCell<Env>) -> Self {
        Self {
            next: None,
            content: ContentEnv(env),
        }
    }

    /// Retourne le pointeur contenu par la cell en le castant en pointeur vers 'Unit'
    pub fn get_ptr(&self) -> *const () {
        match self.content {
            ContentPair(ref pair) => pair.ptr.as_ptr() as *const (),
            ContentEnv(ref env) => env.ptr.as_ptr() as *const (),
        }
    }

    /// Retourne une référence vers les données nécessaires au GC
    /// 
    /// # Safety
    ///  Si déjà free, peut mener à déréférencer un dangling pointer !
    pub unsafe fn gc_ref(&self) -> &GCData {
        match self.content {
            ContentPair(ref pair) => pair.gc_ref(),
            ContentEnv(ref env) => env.gc_ref(),
        }
    }

    /// Retourne une référence mutable vers les données nécessaires au GC
    /// 
    /// # Safety
    ///  Si déjà free, peut mener à déréférencer un dangling pointer !
    pub unsafe fn gc_mut(&mut self) -> &mut GCData {
        match self.content {
            ContentPair(ref mut pair) => pair.gc_mut(),
            ContentEnv(ref mut env) => env.gc_mut(),
        }
    }

    /// Free la pair ou l'env pointé par la cell
    /// 
    /// # Safety
    ///  Si déjà free, peut mener à un double free !
    pub unsafe fn delete(&mut self) {
        match self.content {
            ContentPair(ref mut content) => {
                *content.get_mut() = Pair::default();
                content.free()
            }
            ContentEnv(ref mut content) => {
                *content.get_mut() = Env::default();
                content.free()
            }
        }
    }
}

impl LinkedNext for Box<GCCell> {
    fn next_ref(&self) -> &Option<Self> {
        &self.next
    }

    fn next_mut(&mut self) -> &mut Option<Self> {
        &mut self.next
    }

    fn take_next(&mut self) -> Option<Self> {
        self.next.take()
    }
}

/// Le derive sert à automatiquement implémenter un trait pour type.
#[derive(Clone, Copy)]
pub enum GCCellContent {
    ContentPair(MemCell<Pair>),
    ContentEnv(MemCell<Env>),
}

use std::fmt::{self, Debug, Formatter};
impl Debug for GCCellContent {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            ContentEnv(env) => Debug::fmt(env, f),
            ContentPair(pair) => Debug::fmt(pair, f),
        }
    }
}
