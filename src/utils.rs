pub fn read_bytecode_from_file(path: &str) -> Vec<isize> {
    use std::fs::File;
    use std::io::{BufReader, Read};

    let file = File::open(path).expect("Cannot open file !");
    let mut reader = BufReader::new(file);

    let mut text = String::new();

    reader
        .read_to_string(&mut text)
        .expect("Cannot read file !");

    let mut values = text.split(' ').map(|s| match s.parse::<isize>() {
        Ok(bc) => bc,
        Err(..) => panic!("Cannot read bytecode : '{}' !", s),
    });

    let magic_number = values.next();

    if magic_number != Some(424242) {
        panic!("Wrong magic number : '{:?}' !", magic_number);
    }

    if let Some(n_bc) = values.next() {
        values.take(n_bc as usize).collect()
    } else {
        panic!("Error, missing number of instructions !")
    }
}
