use crate::prelude::*;
use std::fmt::{self, Debug, Formatter};

pub struct Frame {
    pub env: MemCell<Env>,
    pub sp: usize,
    pub pc: usize,
    pub caller_frame: Option<Box<Frame>>,
}

impl Frame {
    fn print_chained(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{{{:?}, pc : {}, sp : {}}}", self.env, self.pc, self.sp)?;
        match self.caller_frame {
            Some(ref next) => {
                write!(f, " => ")?;
                next.print_chained(f)
            }
            None => Ok(()),
        }
    }
}

impl Debug for Frame {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Frame : ")?;
        self.print_chained(f)
    }
}

impl LinkedNext for Box<Frame> {
    fn next_ref(&self) -> &Option<Self> {
        &self.caller_frame
    }

    fn next_mut(&mut self) -> &mut Option<Self> {
        &mut self.caller_frame
    }

    fn take_next(&mut self) -> Option<Self> {
        self.caller_frame.take()
    }
}
