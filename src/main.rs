use rust_svm::prelude::*;

fn main() {
    let mut debug_vm = false;
    let mut debug_gc = false;
    let mut gc_freq: Option<usize> = None;
    let mut path: Option<String> = None;
    for arg in std::env::args().skip(1) {
        match arg.as_ref() {
            "--vmdebug" | "-d" => debug_vm = true,
            "--gcdebug" => debug_gc = true,
            s if s.starts_with("--gcfreq=") => {
                gc_freq = Some(
                    s.strip_prefix("--gcfreq=")
                        .unwrap()
                        .parse::<usize>()
                        .expect("Cannot read frequence"),
                );
            }
            s if s.starts_with('-') => panic!("Unknown option : {}", s),
            s if path.is_none() => path = Some(s.to_string()),
            s if s.is_empty() => (),
            _ => panic!("Too many arguments !"),
        }
    }

    if path == None {
        panic!("No file given !")
    }

    let program = read_bytecode_from_file(&path.unwrap());


    if debug_vm {
        println!("=== Loaded program:");
        bytecode_print(&program);
        println!("===================");
    }

    let mut vm = VM::new(
        program,
        debug_vm,
        debug_gc,
        gc_freq.unwrap_or(VM::DEFAULT_GC_FREQUENCY),
    );

    vm.execute();
}
