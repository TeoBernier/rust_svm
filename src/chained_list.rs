
/**
 * Important :
 * 
 * On utilise beaucoup Option::take :
 *  * self.head.take()
 *  * généralement : elem.take_next() (dépends de l'implémentation pour chaque type)
 * 
 * En Rust, tous les objets ne sont pas copiable (shallow copy) car ils peuvent utiliser des zones mémoires qu'ils modifient.
 * 
 * Par exemple, les String ont un pointeur vers une buffer de caractères modifiables, 
 * copier l'adresse vers ce buffer pourrait alors amené des comportements indéfinis 
 * comme des collisions, double free, et etc...
 * 
 * Ainsi, les types non copiable peuvent potentiellement se cloner, mais à un certain coup (deep copy).
 * 
 * Les options (wrapper autour d'un type),
 *      enum Option<TYPE> { Some(TYPE), None }
 * 
 * prennent certains traits du type qu'elle wrap, comme Copy et Clone. 
 * 
 * Ici, le next que le type contient doit être modifiable, on ne peut alors copier le type, 
 * ainsi, pour une utilisation classique en rust, l'option wrappant ce type ne sera pas copiable non plus.
 * 
 * Il ne sera donc pas possible de faire :
 *      let next = self.head;
 * car le pointeur n'est pas copiable.
 * 
 * Ainsi, le seul moyen de prendre le pointeur ( Some(pointeur) ) est de remplacer la copie originelle par None.
 * 
 * C'est exactement ce que fait take().
 * 
 * Ce code est inimplémentable en rust classique, car le compilateur ne permet pas de copier le contenu de l'option.
 * C'est donc une fonction utilisant du code 'unsafe' (que le compilateur ne regardera pas et dont le programmeur certifie que ça fonctionne)
 * 
 * On peut trouver ce code dans la bibliothèque standard de rust ([std::mem::take](https://doc.rust-lang.org/src/core/mem/mod.rs.html#761)).
 * 
 */

/// Trait pour les types pointant vers un élément suivant.
///
/// auto implement une liste wrappant le type qui peut-être utilisée avec :
/// * iter
/// * into_iter
/// * from_iter
/// * drain_filter
pub trait LinkedNext: Sized {
    fn next_ref(&self) -> &Option<Self>;
    fn next_mut(&mut self) -> &mut Option<Self>;
    fn take_next(&mut self) -> Option<Self>;
}

/// Liste wrappant un type générique ayant le trait LinkedNext
pub struct List<T: LinkedNext> {
    pub head: Option<T>,
}

impl<T: LinkedNext> Default for List<T> {
    fn default() -> Self {
        List { head: None }
    }
}

impl<T: LinkedNext> List<T> {
    /// Crée une liste vide
    pub fn new() -> Self {
        List { head: None }
    }

    /// Change self en une liste vide et renvoie l'ancienne
    pub fn take_list(&mut self) -> List<T> {
        List {
            head: self.head.take(),
        }
    }

    /// Retourne une référence sur le premier item de la liste
    /// 
    /// **La liste ne doit pas être vide**
    pub fn utop(&self) -> &T {
        match self.head {
            Some(ref head) => head,
            None => panic!("List should not be empty"),
        }
    }

    /// Retourne une référence mutable sur le premier item de la liste
    /// 
    /// **La liste ne doit pas être vide**
    pub fn utop_mut(&mut self) -> &mut T {
        match self.head {
            Some(ref mut head) => head,
            None => panic!("List should not be empty"),
        }
    }

    /// Push un élément au début de la liste (LIFO)
    pub fn push(&mut self, mut elem: T) {
        *elem.next_mut() = self.head.take();
        self.head = Some(elem);
    }

    /// Pop le premier élément de la liste et le renvoie
    pub fn pop(&mut self) -> Option<T> {
        match self.head.take() {
            Some(mut elem) => {
                self.head = elem.take_next();
                Some(elem)
            }
            None => None,
        }
    }

    /// Pop le premier élément de la liste et le renvoie
    /// 
    /// **La liste ne doit pas être vide**
    pub fn upop(&mut self) -> T {
        match self.head.take() {
            Some(mut elem) => {
                self.head = elem.take_next();
                elem
            }
            None => panic!("List should not be empty"),
        }
    }

    /// Crée un itérateur enlevant les éléments, respectant une propriété, de la liste initiale et les renvoie.
    pub fn drain_filter<F: Fn(&T) -> bool>(&mut self, f: F) -> DrainIter<T, F> {
        DrainIter::new(&mut self.head, f)
    }

    /// Crée un itérateur renvoyant des références sur les éléments de la liste
    pub fn iter(&self) -> impl Iterator<Item = &T> {
        LinkedIter(self.head.as_ref())
    }
}

use std::iter::FromIterator;
impl<T: LinkedNext> FromIterator<T> for List<T> {
    /// Crée une liste depuis n'importe quel type pouvant s'itérer, sans changer l'ordre.
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = T>,
    {
        let mut list = List::new();
        let mut prev = &mut list.head;
        for elem in iter {
            *prev = Some(elem);
            prev = prev.as_mut().unwrap().next_mut();
        }

        list
    }
}

impl<T: LinkedNext> Drop for List<T> {
    /// La désallocation est automatique en Rust.
    /// Ici, le compilateur désallouera la liste par récursion (essayant de désallouer le next).
    /// 
    /// Or, si la liste est trop grande, il se peut que l'on ait un stack overflow.
    /// Il faut donc désallouer les éléments de liste itérativement en les popant un par un.
    fn drop(&mut self) {
        while self.pop().is_some() {}
    }
}

/// Un tuple `(Some(type_implémentant_LinkedNext), fonction_checkant_la_propriété)`
/// 
/// Ici on est obligé d'utiliser une option (même si on sait que le contenu de la structure sera toujours Some(..))
/// à cause d'un problème de lifetime (notion permettant la désallocation par le compilateur).
pub struct DrainIter<'a, T: LinkedNext, F: Fn(&T) -> bool>(Option<&'a mut Option<T>>, F);

impl<'a, T: LinkedNext, F: Fn(&T) -> bool> DrainIter<'a, T, F> {
    fn new(head: &'a mut Option<T>, f: F) -> Self {
        DrainIter(Some(head), f)
    }
}

impl<'a, T: LinkedNext, F: Fn(&T) -> bool> Iterator for DrainIter<'a, T, F> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        while let Some(current @ Some(..)) = self.0.take() {
            if self.1(current.as_ref().unwrap()) {
                let mut elem = (current).take().unwrap();
                *current = elem.take_next();
                self.0 = Some(current);
                return Some(elem);
            } else {
                self.0 = Some(current.as_mut().unwrap().next_mut());
            }
        }
        None
    }
}

pub struct LinkedIter<'a, T: LinkedNext>(Option<&'a T>);

impl<'a, T: LinkedNext> Iterator for LinkedIter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<&'a T> {
        match self.0 {
            Some(elem) => {
                self.0 = elem.next_ref().as_ref();
                Some(elem)
            }
            None => None,
        }
    }
}

pub struct LinkedIntoIter<T: LinkedNext>(Option<T>);

impl<T: LinkedNext> Iterator for LinkedIntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        match self.0.take() {
            Some(mut elem) => {
                self.0 = elem.take_next();
                Some(elem)
            }
            None => None,
        }
    }
}

impl<T: LinkedNext> IntoIterator for List<T> {
    type Item = T;
    type IntoIter = LinkedIntoIter<T>;

    fn into_iter(mut self) -> LinkedIntoIter<T> {
        LinkedIntoIter(self.head.take())
    }
}
