# Rust Svm

## How to use :

* [install](https://www.rust-lang.org/tools/install) rust (should be packed with rustup (version manager for rust) and cargo (package manager))
  * On windows, you have to install the msvc, read carefully.
* you have to use rust nightly, so download it with :
  * `rustup toolchain install nightly --profile default`
    * If it does not work : `rustup toolchain install nightly --profile minimal`
* go in the project root
* `rustup override set nightly`
* run with `cargo run filename`

