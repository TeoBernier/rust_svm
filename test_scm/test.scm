
(define (map f l) 
    (if (null? l)
        (list)
        (cons (f (car l)) (map f (cdr l)))))

(define (reduce f b l)
    (if (null? l)
        b
        (reduce f (f (car l) b) (cdr l))))

(define (reverse l)
    (reduce (lambda (el nl) (cons el nl)) (list) l))

(define (rever-mut l)
    (define (loop nl l)
        (if (null? l)
            nl
            (let ((next (cdr l)))
                (set-cdr! l nl)
                (loop l next))))
    (loop (list) l))

(define (interval lower upper)
    (define (loop l n)
        (if (= n lower)
            (cons n l)
            (loop (cons n l) (- n 1))))
    (loop (list) upper))

(rever-mut (interval 0 10))
